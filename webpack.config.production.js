const path = require("path");

module.exports = {
    mode: "production",
    entry: "./src/js/game.js",  
    output: {
        path: path.resolve(__dirname, "build"),
        filename: "main.js",
    },
    module: {
        rules: [{
            rules: [
                {
                  test: /\.css$/,
                  use: ['style-loader', 'css-loader']
                }
            ],
        }]
    }
}
