// class des actions 

export class Spell {

    constructor(name, damage, sentence, limitSort) {
        this.name = name;
        this.damage = damage;
        this.sentence = sentence;
        this.limitSort = limitSort;
    }
}