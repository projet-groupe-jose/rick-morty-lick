import { Psentence } from "./classP.js";

export class playerDamage {
    constructor(life, damageLife, sentence) {
        this.life = life;
        this.damageLife = damageLife;
        this.sentence = sentence;
    }

    funcAttack(target) {
        target.life = this.life - this.damageLife;
        let p = new Psentence("Player: " + this.sentence);
        p.Psentence();
        document.querySelector("#life_computer").textContent = target.life;
        document.querySelector("#progressAI").value = target.life;
    }
}